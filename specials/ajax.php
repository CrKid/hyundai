<? 	
	require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
	CModule::IncludeModule('iblock');
	
	if ($_POST) {
		
		require $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/main/include/PHPMailer.php';
		$mailer = new PHPMailer;
		
		$mailer->CharSet = 'utf-8';
		$mailer->setFrom('form_sender@yug-avto.ru', 'Формы сайта HYUNDAI. Лэндинг Продажи.');
		$mailer->Subject = 'Заявка с лэндинга Hyundai Продажи. Форма: "'.$_POST['Form'].'"';
		
		$rs = CIBlockElement::GetProperty(15, 22017, [], ['CODE' => 'FORM_'.$_POST['Section']]);
		while ($ob = $rs->GetNext()) $recs[] = $ob['VALUE'];

//		YApp::sp($_POST);		
//		YApp::sp($recs); die;
		
		foreach ( $recs as $r ) {
			
			$r = ( $r == 'callcenter@yug-avto.ru' ) ?: 'callcenter@adv.yug-avto.ru';
			$mailer->addAddress(trim($r), '');
		}
		
//		$mailer->addAddress('anton.boreckiy@yug-avto.ru', '');
		$mailer->addAddress('elena.asatryan@yug-avto.ru', '');
		
		$m    = 'Заполнена форма "'.$_POST['Form'].'"<br /><br />';
		
		if ( $_POST['Name'] ) $m .= '<strong>Имя</strong>: '.$_POST['Name'].'<br />';
		if ( $_POST['Phone'] ) $m .= '<strong>Телефон</strong>: '.$_POST['Phone'].'<br />';
		if ( $_POST['Email'] ) $m .= '<strong>Email</strong>: '.$_POST['Email'].'<br />';
		if ( $_POST['DC'] ) $m .= '<strong>Дилерский центр</strong>: '.$_POST['DC'].'<br /><br />';
		
		if ( $_POST['Car'] ) $m .= '<strong>Автомобиль</strong>: '.$_POST['Car'].'<br />';
		if ( $_POST['Year'] ) $m .= '<strong>Год выпуска</strong>: '.$_POST['Year'].'<br />';
		if ( $_POST['Milleage'] ) $m .= '<strong>Пробег</strong>: '.$_POST['Milleage'].'<br />';
		if ( $_POST['Work'] ) $m .= '<strong>Работы</strong>: '.$_POST['Work'].'<br />';
		
		if ( $_POST['Comment'] ) $m .= '<br /><strong>Комментарий</strong>:<br />'.$_POST['Comment'].'<br />';
		
		if ( $_POST['Section'] ) {
			
			$arIns = [
				'MODIFIED_BY' => 1,
				'IBLOCK_ID' => 17,
				'IBLOCK_SECTION_ID' => (int)$_POST['Section'],
				'NAME' => $_POST['Name'].'. '.$_POST['Form'].'.',
				'ACTIVE' => 'Y',
				'DETAIL_TEXT' => $m,
				'PROPERTY_VALUES' => [
					'NAME' => $_POST['Name'],
					'PHONE' => $_POST['Phone']
				]
			];
			
			if ( (int)$_POST['Section'] == 11 ) {
				
//				$mailer->addAddress('anton.boreckiy@yug-avto.ru', '');
			}
		}
		
		$el = new CIBlockElement;
		
		// создадим новый результат
		if ( $PRODUCT_ID = $el->Add($arIns) ) {
			
			$mailer->msgHTML($m);
			$mailer->Send();
			
			$arRes = ['status' => 'success', 'post'=>$_POST, 'recs'=>$recs];
			
		} else {
			
			$arRes = ['status' => 'error'];
		}
		
		echo (isset($_POST['callback']) ? $_POST['callback'] : '') . json_encode($arRes);
	}
?>