
    <div class="container my-5 footer">
      <div class="row">
        <div class="col-md-12">
          <small><?=$GLOBALS['SETTINGS']['CONTENT']['DETAIL_TEXT']?></small>
        </div>
        <div class="col-md-12"></div>
      </div>
    </div>
	
    <? include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/_svg.php'; ?>
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
    
    <script src="<?=SITE_TEMPLATE_PATH?>/assets/js/app.js?<?=md5_file($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/assets/js/app.js');?>"></script>
    
    <? include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/_scripts.php'; ?>
  
  </body>
</html>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>