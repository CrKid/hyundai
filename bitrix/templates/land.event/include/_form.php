<div class="container py-5">
  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
      <?php //  YApp::sp( $GLOBALS['SETTINGS'] ); ?>
      <h1 class="text-center font-weight-normal"><?=(($GLOBALS['SETTINGS']['CITY']!='nvr')?$GLOBALS['SETTINGS']['CONTENT']['~PROPERTY_EVENT_H1_VALUE']:'')?></h1>
      <h2 class="text-center font-weight-normal"><?=$GLOBALS['SETTINGS']['CONTENT']['~PROPERTY_EVENT_FORM_TITLE_VALUE']?></h2>
      <h4 class="text-center font-weight-normal"><?=$GLOBALS['SETTINGS']['CONTENT'][$GLOBALS['SETTINGS']['CONTENT']['FORM_TEXT']]['TEXT']?></h4>
      <form class="my-5" data-event="event">
        <input type="hidden" name="Form" value="Запись на мероприятие" />
        <input type="hidden" name="Section" value="17" />
        <div class="row text-left">
          <div class="col-md-12">
            <div class="form-group">
              <input type="text" class="form-control" name="Name" placeholder="ФИО *" required />
            </div>
            <div class="form-group">
              <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
            </div>
            <div class="form-group">
              <select class="form-control" name="DC" required>
                <option disabled selected>Дилерский центр *</option>
                <? foreach ( $GLOBALS['SETTINGS']['DC'] as $k => $i ) { ?>
                <?php if ($k!=1) { ?><option value="<?=$i['NAME']?>" <?=((count($GLOBALS['SETTINGS']['DC'])==1)?'selected':'')?>><?=$i['NAME']?></option><?php } ?>
                <? } ?>
              </select>
            </div>
            <div class="form-group my-3">
              <a href="<?//=CFile::GetPath($GLOBALS['SETTINGS']['CONTENT'][$GLOBALS['SETTINGS']['CONTENT']['TICKET']])?>#" <? /*download */ ?> class="but-darkblue btn-block p-2 text-center" role="Send"><?=(($GLOBALS['SETTINGS']['CONTENT']['PROPERTY_EVENT_FORM_BUTTON_VALUE'])?:'Отправить')?></a>
            </div>
            <div class="form-group">
              <p><small>Поля, отмеченные *, обязательны для заполнения.<br />Сообщая данные сведения, я даю согласие на обработку персональных данных и рекламную коммуникацию.</small></p>
            </div>
          </div>
        </div>
      </form>
      <div class="alert alert-dismissible alert-success">
        <strong>Спасибо за вашу заявку!</strong> Мы свяжемся с Вами в ближайшее время.
      </div>
      <div class="alert alert-dismissible alert-danger">
        <strong>Ой, что-то пошло не так!</strong> Повторите попытку позднее.
      </div>
    </div>
    <div class="col-md-2"></div>
    <div class="col-md-2"></div>
    <div class="col-md-8"><?=$GLOBALS['SETTINGS']['CONTENT'][$GLOBALS['SETTINGS']['CONTENT']['MAP']]['TEXT']?></div>
    <div class="col-md-2"></div>
  </div>
</div>

<?php // YApp::sp($GLOBALS['SETTINGS']['DC']); ?>