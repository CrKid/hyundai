<div class="container futures my-5">
  <div class="row">
    <div class="col-md-12"><h2 class="text-center mb-5 title">Наши партнеры</h2></div>
  </div>
  <div class="row py-5">
  	<?php foreach ( $GLOBALS['SETTINGS']['CONTENT']['PARTNERS'] as $item) { ?>
    <div class="col-md-2">
      <img class="w-100" src="<?=CFile::GetPath($item)?>" />
    </div>
    <?php } ?>
  </div>
</div>