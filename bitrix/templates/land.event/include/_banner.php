	<script>
    	JSApp.Timer.Time = {};
		<? foreach ( $GLOBALS['SETTINGS']['CONTENT']['TIMER'] as $k => $v ) { ?>
		JSApp.Timer.Time.<?=$k?> = Number(<?=$v?>);
		<? } ?>
    </script>
	<div class="banner">
      <div class="container">
        <div class="row pb-4">
          <div class="col banner-img position-relative">
            <img class="w-100 pc"
            	alt="<? $APPLICATION->ShowTitle(); ?>"
                src="<?=CFile::GetPath((($GLOBALS['SETTINGS']['CONTENT']['PROPERTY_BANNER_PC_'.mb_strtoupper($GLOBALS['SETTINGS']['CITY']).'_VALUE'])?:$GLOBALS['SETTINGS']['CONTENT']['DETAIL_PICTURE']))?>" />
            <img class="w-100 mob"
            	alt="<? $APPLICATION->ShowTitle(); ?>"
                src="<?=CFile::GetPath((($GLOBALS['SETTINGS']['CONTENT']['PROPERTY_BANNER_M_'.mb_strtoupper($GLOBALS['SETTINGS']['CITY']).'_VALUE'])?:$GLOBALS['SETTINGS']['CONTENT']['PREVIEW_PICTURE']))?>" />
          </div>
        </div>
        
        <? if ( $GLOBALS['SETTINGS']['CONTENT']['PROPERTY_COUNTDOWN_USE_VALUE'] && $GLOBALS['SETTINGS']['CONTENT']['TIMER']['RAW'] ) { ?>
        <div class="row pb-2 timer pc">
          <div class="col-4"></div>
          <div class="col-4 text-center"><strong><?=$GLOBALS['SETTINGS']['CONTENT']['PROPERTY_COUNTDOWN_TEXT_VALUE']?></strong></div>
          <div class="col-4"></div>
        </div>
        <div class="row timer timer-value pc">
          <div class="col-4"></div>
          <div class="col-1 text-center d"><?=$GLOBALS['SETTINGS']['CONTENT']['TIMER']['DAYS']?></div>
          <div class="col-1 text-center h"><?=$GLOBALS['SETTINGS']['CONTENT']['TIMER']['HOURS']?></div>
          <div class="col-1 text-center m"><?=$GLOBALS['SETTINGS']['CONTENT']['TIMER']['MINUTS']?></div>
          <div class="col-1 text-center s"><?=$GLOBALS['SETTINGS']['CONTENT']['TIMER']['SECONDS']?></div>
          <div class="col-4"></div>
        </div>
        <div class="row pb-4 timer timer-desc pc">
          <div class="col-4"></div>
          <div class="col-1 text-center d"><?=YApp::getWorld( $GLOBALS['SETTINGS']['CONTENT']['TIMER']['DAYS'], 'd' )?></div>
          <div class="col-1 text-center h"><?=YApp::getWorld( $GLOBALS['SETTINGS']['CONTENT']['TIMER']['HOURS'], 'h' )?></div>
          <div class="col-1 text-center m"><?=YApp::getWorld( $GLOBALS['SETTINGS']['CONTENT']['TIMER']['MINUTS'], 'm' )?></div>
          <div class="col-1 text-center s"><?=YApp::getWorld( $GLOBALS['SETTINGS']['CONTENT']['TIMER']['SECONDS'], 's' )?></div>
          <div class="col-4"></div>
        </div>
        <div class="row pb-2 timer mob">
          <div class="col-2"></div>
          <div class="col text-center"><strong><?=$GLOBALS['SETTINGS']['CONTENT']['PROPERTY_COUNTDOWN_TEXT_VALUE']?></strong></div>
          <div class="col-2"></div>
        </div>
        <div class="row timer timer-value mob">
          <div class="col-1"></div>
          <div class="col text-center d"><?=$GLOBALS['SETTINGS']['CONTENT']['TIMER']['DAYS']?></div>
          <div class="col text-center h"><?=$GLOBALS['SETTINGS']['CONTENT']['TIMER']['HOURS']?></div>
          <div class="col text-center m"><?=$GLOBALS['SETTINGS']['CONTENT']['TIMER']['MINUTS']?></div>
          <div class="col text-center s"><?=$GLOBALS['SETTINGS']['CONTENT']['TIMER']['SECONDS']?></div>
          <div class="col-1"></div>
        </div>
        <div class="row pb-4 timer timer-desc mob">
          <div class="col-1"></div>
          <div class="col text-center d"><?=YApp::getWorld( $GLOBALS['SETTINGS']['CONTENT']['TIMER']['DAYS'], 'd' )?></div>
          <div class="col text-center h"><?=YApp::getWorld( $GLOBALS['SETTINGS']['CONTENT']['TIMER']['HOURS'], 'h' )?></div>
          <div class="col text-center m"><?=YApp::getWorld( $GLOBALS['SETTINGS']['CONTENT']['TIMER']['MINUTS'], 'm' )?></div>
          <div class="col text-center s"><?=YApp::getWorld( $GLOBALS['SETTINGS']['CONTENT']['TIMER']['SECONDS'], 's' )?></div>
          <div class="col-1"></div>
        </div>
        <? } ?>
        
      </div>
    </div>