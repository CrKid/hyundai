'use strict';

Array.prototype.in_array = function(p_val) {
	for(var i = 0, l = this.length; i < l; i++)	{
		if(this[i] == p_val) {
			return true;
		}
	}
	return false;
}


JSApp.ChangeSlide = function() {
	
	var prevID = Number($('div.swiper-slide-prev').data('slide')) + 1; if ( prevID == 6 ) prevID = 0;
	var nextID = Number($('div.swiper-slide-next').data('slide')) + 1; if ( nextID == 6 ) nextID = 0;
	
	var prevImage = $('div.swiper-slide[data-slide="'+prevID+'"]').data('image');
	var nextImage = $('div.swiper-slide[data-slide="'+nextID+'"]').data('image');
	
	var prevName = $('div.swiper-slide[data-slide="'+prevID+'"]').data('name');
	var nextName = $('div.swiper-slide[data-slide="'+nextID+'"]').data('name');
	
	$('div.prev-pic').children('img').attr('src', prevImage);
	$('div.next-pic').children('img').attr('src', nextImage);
	
	$('div.prev-pic').children('h3').text(prevName);
	$('div.next-pic').children('h3').text(nextName);
	
	setTimeout( function() {
		
		$('a#offer').data('car', 'Hyundai '+$('div.swiper-slide-active').data('name') );
		$('.swiper-bg').css('background-image', 'url('+$('div.swiper-slide-active').data('bg')+')');
		$('.but-petal').html( $('div.swiper-slide-active').data('credit') );
	}, 300);
}

JSApp.Timer.getWorld = function( q, f ) {
	
	var res = {};
	res.d = ['день', 'дня', 'дней'];
	res.h = ['час', 'часа', 'часов'];
	res.m = ['минута', 'минуты', 'минут'];
	res.s = ['секунда', 'секунды', 'секунд'];
	
	var test = {};
	test.a = [1, 21, 31, 41, 51, 61, 71, 81, 91];
	test.b = [2,3,4,22,23,24,32,33,34,42,43,44,52,53,54,62,63,64,72,73,74,82,83,84,92,93,94];
	
	if ( test.a.in_array(q) ) return res[f][0];
	if ( test.b.in_array(q) ) return res[f][1];
	return res[f][2];
}

JSApp.Timer.Close = function() {
	
	$('.timer').remove();
}

JSApp.Timer.Update = function() {
	
	JSApp.Timer.Time.SECONDS -= 1;
	if ( JSApp.Timer.Time.SECONDS < 0 ) {
		
		JSApp.Timer.Time.SECONDS = 59;
		JSApp.Timer.Time.MINUTS -= 1;
		
		if ( JSApp.Timer.Time.MINUTS < 0 ) {
			
			JSApp.Timer.Time.MINUTS = 59;
			JSApp.Timer.Time.HOURS -= 1;
			
			if ( JSApp.Timer.Time.HOURS < 0 ) {
				
				JSApp.Timer.Time.HOURS = 23;
				JSApp.Timer.Time.DAYS -= 1;
				
				if ( JSApp.Timer.Time.DAYS < 0 ) {
					
					JSApp.Timer.Close();
				}
			}
		}
	}
	
	JSApp.Timer.Render();
}

JSApp.Timer.Render = function() {
	
	$('.timer-value .d').text( JSApp.Timer.Time.DAYS );
	$('.timer-value .h').text( JSApp.Timer.Time.HOURS );
	$('.timer-value .m').text( JSApp.Timer.Time.MINUTS );
	$('.timer-value .s').text( JSApp.Timer.Time.SECONDS );
	
	$('.timer-desc .d').text( JSApp.Timer.getWorld(JSApp.Timer.Time.DAYS, 'd') );
	$('.timer-desc .h').text( JSApp.Timer.getWorld(JSApp.Timer.Time.HOURS, 'h') );
	$('.timer-desc .m').text( JSApp.Timer.getWorld(JSApp.Timer.Time.MINUTS, 'm') );
	$('.timer-desc .s').text( JSApp.Timer.getWorld(JSApp.Timer.Time.SECONDS, 's') );
}




$('input[type="phone"]').inputmask({'mask': '+7 (999) 999-99-99', showMaskOnHover: false});

var swiper = new Swiper('.swiper-container', {
	loop: true,
	spaceBetween: 30
});

swiper.on('slideChange', function() { JSApp.ChangeSlide(); });
$('.prev-pic').click( function() { swiper.slidePrev( 300, JSApp.ChangeSlide() ); return false; });
$('.next-pic').click( function() { swiper.slideNext( 300, JSApp.ChangeSlide() ); return false; });

$('a[role="Offer"], a[role="Credit"]').click( function() {
	
	var Car = ( typeof $(this).data('car') != 'undefined' ) ? $(this).data('car') : '';
	
	var w = $('div[data-remodal-id="offer"]');
	w.find('input[name="Car"]').val( Car );
	w.find('h2[role="Car"]').text( Car );
	
	w = $('div[data-remodal-id="credit"]');
	w.find('input[name="Car"]').val( Car );
	w.find('h2[role="Car"]').text( Car );
});

$('[role="scroll"]').click( function() {
	
	$('html, body').animate({ scrollTop: $('[data-block="' + $(this).data('scroll') + '"]').offset().top }, 500);
});

$('[type="email"]').blur( function() {
	
	if ( $(this).val() && !YApps.CheckEmail( $(this).val() ) ) {
		
		$(this).addClass('is-invalid'); 
		
	} else {
		
		$(this).removeClass('is-invalid'); 
	}
});

setInterval( function() {
	$('.prev-pic svg, .next-pic svg ').addClass('op');
	setTimeout( function() {
		$('.prev-pic svg, .next-pic svg ').removeClass('op');
	}, 500);
	JSApp.Timer.Update();
}, 1000);

$('a[role="Send"]').click( function() {
	
	var Form = $(this).parent().parent().parent().parent();
	
	var success = $(Form).parent().find('.alert-success');
	var error = $(Form).parent().find('.alert-danger');
	var Event = $(Form).data('event');
	
	YApps.SendData.AppName = 'Lands';
	YApps.SendData.EventCategory = 'Заполена форма';
	
	YApps.SendData.Flag = true;
	
	Form.find('input, select, textarea').each( function(i, e) {
		
		YApps.SendData[$(e).attr('name')] = $(e).val();
		$(e).removeClass('is-invalid');
		
		if ( $(e).attr('required') ) {
			
			if ( !$(e).val() ) {
				
				YApps.SendData.Flag = false;
				$(e).addClass('is-invalid');
			}
		}
		
		if ( $(e).is('[type="email"]') ) {
			
			if ( $(e).val() && !YApps.CheckEmail( $(e).val() ) ) {
				
				YApps.SendData.Flag = false;
				$(e).addClass('is-invalid');
			}
		}
	});
	
	if ( YApps.SendData.Flag ) {
		
		$.ajax({
			type: "POST",
			url: '/event/ajax.php',  
			data: YApps.SendData,
			success: function(data){
				
				var res = JSON.parse( data );
				
				if ( res.status == 'success' ) {
					
					$(Form).slideUp(300);
					$(success).slideDown(300);
					$(error).hide();
					
					if ( typeof fbq != 'undefined' ) fbq('track', 'Lead');
					if (typeof yaCounter7783918 != 'undefined') yaCounter7783918.reachGoal('HyundaiLandind_send');
                    if (typeof Matomo != 'undefined') _paq.push(["trackEvent", 'Формы лэндинга', YApps.SendData.Form, 'Отправка']);
						
					var Layer = {
						'event': 'FormSubmission',
						'eventCategory': Event,
						'eventAction': 'submit',
						'eventLabel': YApps.SendData.Car || null
					};
					dataLayer.push( Layer );
				
					var d = ( YApps.SendData.DC == 'Hyundai г. Новороссийск' ) ? '20893' : '20892' ;
					var s = ( YApps.SendData.DC == 'Hyundai г. Новороссийск' ) ? window.call_value_5 : window.call_value_4 ;
					
					var CallTouchURL = 'https://api-node7.calltouch.ru/calls-service/RestAPI/requests/'+d+'/register/';
					CallTouchURL += '?subject=Формы лэндинга - '+YApps.SendData.Form;
					CallTouchURL += '&sessionId='+s;
					CallTouchURL += '&fio='+YApps.SendData.Name;
					CallTouchURL += '&phoneNumber='+YApps.SendData.Phone.replace(/[^\d;]/g, '');
					
					$.get( CallTouchURL );
					
					YApps.AppPushStat( YApps.SendData );
					
				} else {
					
					$(error).slideDown(300);
				}
			},
			error: function() {
				
				$(error).slideUp(300);
			}
		});
	
	} else {
		
		return false;	
	}
});