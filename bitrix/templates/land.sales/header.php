<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); CModule::IncludeModule('iblock'); ?>
<? include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/get_global_settings.php'; ?>
<!doctype html>
<html lang="ru">
  <head>
  	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NG6KHHV');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Boretscy A">
    <title><? $APPLICATION->ShowTitle(); ?></title>

    <!-- Bootstrap core CSS -->
	<link href="<?=SITE_TEMPLATE_PATH?>/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal-default-theme.min.css" />
    
    <?// $APPLICATION->ShowHead()?>
    
    <link href="<?=SITE_TEMPLATE_PATH?>/assets/fonts/fonts.css?<?=md5_file($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/assets/fonts/fonts.css')?>" rel="stylesheet">
    <link href="<?=SITE_TEMPLATE_PATH?>/assets/css/app.css?<?=md5_file($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/assets/css/app.css')?>" rel="stylesheet">
    
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://hyundai.yug-avto.ru/"; />
    <meta property="og:title" content="<? $APPLICATION->ShowTitle(); ?>" />
    <meta property="og:description" content="<? $APPLICATION->ShowTitle(); ?>" />
    <meta property="og:image" content="http://hyundai.yug-avto.ru/bitrix/templates/main/assets/images/og_logo.jpg"  />
    
    <script src="<?=SITE_TEMPLATE_PATH?>/assets/js/init.js?<?=md5_file($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/assets/js/init.js');?>"></script>
    
    <!-- Facebook Pixel Code -->
	<script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '386535742186539');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=386535742186539&ev=PageView&noscript=1"/></noscript>
    <!-- End Facebook Pixel Code -->

    
  </head>
  <body>
  	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NG6KHHV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	<? // $APPLICATION->ShowPanel();?>
    <? include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/_counters.php'; ?>
	
    <div class="top-row bg-lightgray">
      <div class="container">
        <div class="row py-3">
          <div class="col col-md-3 pt-2 logo-hyundai-yugavto">
            <a href="/specials/"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-LogoHyundaiYugavto"></use></svg></a>
          </div>
          <div class="col text-right">
            <ul class="p-0 m-0">
              <li class="d-inline-block ml-4 pc"><img src="<?=SITE_TEMPLATE_PATH?>/assets/images/dealer.png" style="width:200px;" /></li>
              <li class="d-inline-block ml-4 pc"><img src="<?=SITE_TEMPLATE_PATH?>/assets/images/ya.png" style="width:200px;" /></li>
              
			  <? switch ( $GLOBALS['SETTINGS']['CITY'] ) {
			
				  case 'krd':
					  ?>
                      <li class="d-inline-block ml-4"><span class="mr-2 pc">Краснодар</span> <a href="tel:<?=YApp::formatPhoneIn($GLOBALS['SETTINGS']['PHONE'][0])?>"><?=YApp::formatPhoneOut($GLOBALS['SETTINGS']['PHONE'][0])?></a></li>
                      <?
					  break;
					  
				  case 'nvr':
					  ?>
                      <li class="d-inline-block ml-4"><span class="mr-2 pc">Новороссийск</span> <a href="tel:<?=YApp::formatPhoneIn($GLOBALS['SETTINGS']['PHONE'][1])?>"><?=YApp::formatPhoneOut($GLOBALS['SETTINGS']['PHONE'][1],true)?></a></li>
                      <?
					  break;
				  
				  default:
				  	  ?>
                      <li class="d-inline-block ml-4"><span class="mr-2 pc">Краснодар</span> <a href="tel:<?=YApp::formatPhoneIn($GLOBALS['SETTINGS']['PHONE'][0])?>"><?=YApp::formatPhoneOut($GLOBALS['SETTINGS']['PHONE'][0])?></a></li>
                      <li class="d-inline-block ml-4"><span class="mr-2 pc">Новороссийск</span> <a href="tel:<?=YApp::formatPhoneIn($GLOBALS['SETTINGS']['PHONE'][1])?>"><?=YApp::formatPhoneOut($GLOBALS['SETTINGS']['PHONE'][1],true)?></a></li>
                      <?
					  break;
			  } ?>
              <li class="d-inline-block ml-4 pc"><a href="#" role="scroll" data-scroll="contacts"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-CapretDown"></use></svg></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    
    <div class="logo-row pc">
      <div class="container">
        <div class="row py-4">
          <div class="col text-right pc" style="display: block;">
          	<ul class="p-0 m-0">
              <li class="d-inline-block ml-4"><a href="#hot" role="scroll" data-scroll="hot"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-Car"></use></svg> Горячие предложения</a></li>
			  <li class="d-inline-block ml-4"><a href="#offers" role="scroll" data-scroll="offers"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-Like"></use></svg> Спецпредложения</a></li>
			  <li class="d-inline-block ml-4"><a href="#credit" role="scroll" data-scroll="credit"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-Cards"></use></svg> Кредит</a></li>
			  <li class="d-inline-block ml-4"><a href="#tradein"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-CarRefresh"></use></svg> Trade In</a></li>
			  <li class="d-inline-block ml-4"><a href="#testdrive"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-Steering"></use></svg> Тест-драйв</a></li>
              <li class="d-inline-block ml-4"><a href="#service"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-Wrench"></use></svg> Сервис</a></li>
              <li class="d-inline-block ml-4"><a href="#contacts" role="scroll" data-scroll="contacts"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-Map"></use></svg> Контакты</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    
    <?php include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/_banner.php'; ?>
    
    <?php
	  $arFilter = ['IBLOCK_ID'=>16, 'ACTIVE_DATE'=>'Y', 'ACTIVE'=>'Y'];
	  $arSelect = ['ID', 'NAME', 'PROPERTY_ICON', 'PROPERTY_LINK', 'PROPERTY_ANCHOR'];
	  $arRes = [];
	  $res = CIBlockElement::GetList(['SORT'=>'ASC'], $arFilter, false, false, $arSelect);
	  while ($ob = $res->GetNextElement()) $arRes[] = $ob->GetFields();
	?>
	
    <div class="btn-row bg-darkblue mob">
      <div class="container">
        <?php /*
		<div class="row py-4 pc">
		  
          <? foreach ( $arRes as $arItem ) { ?>
          <div class="col-md-2">
            <a class="pl-5 btn-block <?=((strlen($arItem['NAME'])<11)?'pt-2':'')?>" href="<?=$arItem['PROPERTY_LINK_VALUE']?>" <?=(($arItem['PROPERTY_ANCHOR_VALUE'])?'role="scroll" data-scroll="'.$arItem['PROPERTY_ANCHOR_VALUE'].'"':'')?>>
              <svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-<?=$arItem['PROPERTY_ICON_VALUE']?>"></use></svg> <?=$arItem['NAME']?>
            </a>
          </div>
          <? } ?>
		  
        </div>
		*/ ?>
        <div class="row mob">
          <div class="col-md-12">
            <ul class="p-0">
              <? foreach ( $arRes as $arItem ) { ?>
              <li class="my-4">
                <a class="pl-5 pt-3 btn-block" href="<?=$arItem['PROPERTY_LINK_VALUE']?>" <?=(($arItem['PROPERTY_ANCHOR_VALUE'])?'role="scroll" data-scroll="'.$arItem['PROPERTY_ANCHOR_VALUE'].'"':'')?>>
                  <svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-<?=$arItem['PROPERTY_ICON_VALUE']?>"></use></svg> <?=$arItem['NAME']?>
                </a>
              </li>
              <? } ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
	  
	<?php include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/_slider.php'; ?>
	
	<?php include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/_hot.php'; ?>
	  
    
    <?php include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/_credit.php'; ?>
    
    
    
    <?php /*
    <div class="container-fluid my-5 py-5" data-block="available">
      <iframe src="https://cars.hyundai.yug-avto.ru/?iframe<?=(($GLOBALS['SETTINGS']['CITY']=='nvr')?'&City=novorossijsk':'')?>" style="width: 100%; min-height: 1750px;"></iframe>
    </div>
	*/ ?>
	
    <? include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/_futures.php'; ?>
    
    <div class="position-relative bg-darkblue" data-block="contacts">
      
      <div class="container">
        <div class="row">
          <div class="col-12 p-4 contacts">
            <svg class="logo" xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-LogoYugavto"></use></svg>
            <h5 class="py-2">Официальный дилер Hyundai</h5>
            <ul class="p-0 my-3">
              <? foreach ( $GLOBALS['SETTINGS']['DC'] as $i ) { ?>
              <li class="py-2">
                <h5 class="py-2"><?=$i['NAME']?></h5>
                <p><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-Map"></use></svg> <?=$i['PROPERTY_ADDRESS_VALUE']?>
                <br />
                  <svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-Phone"></use></svg> 
                  <a href="tel:<?=YApp::formatPhoneIn($i['PROPERTY_PHONE_VALUE'])?>"><?=YApp::formatPhoneOut($i['PROPERTY_PHONE_VALUE'])?></a></p>
              </li>
              <? } ?>
            </ul>
          </div>
        </div>
      </div>
      
      
      
      <? switch ( $GLOBALS['SETTINGS']['CITY'] ) {
			
		  case 'krd':
			  ?>
			  <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A4f378f4ca954668b1f17589e8c4236e3b753f8cd79c34cabd55da4eb3ba0215c&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>
			  <?
			  break;
			  
		  case 'nvr':
			  ?>
			  <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Abe15720977a7be25a11a6f10abc5a1b301212aeceb8608ee6b0cfbbbb1fd4ffe&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>
			  <?
			  break;
		  
		  default:
			  ?>
			  <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3AbvYl8ki_R0aftJ8W1JqDbyRDz_w6TnaJ&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>
			  <?
			  break;
	  } ?>
    </div>