<?php 
	if ( $GLOBALS['SETTINGS']['CITY'] ) {
		
		echo $GLOBALS['SETTINGS']['CONTENT']['~PROPERTY_CALLTOUCH_'.strtoupper($GLOBALS['SETTINGS']['CITY']).'_VALUE']['TEXT'];
		
	} else {
		
		?>
        <script type="text/javascript">
(function(w,d,n,c){w.CalltouchDataObject=n;w[n]=function(){w[n]["callbacks"].push(arguments)};if(!w[n]["callbacks"]){w[n]["callbacks"]=[]}w[n]["loaded"]=false;if(typeof c!=="object"){c=[c]}w[n]["counters"]=c;for(var i=0;i<c.length;i+=1){p(c[i])}function p(cId){var a=d.getElementsByTagName("script")[0],s=d.createElement("script"),i=function(){a.parentNode.insertBefore(s,a)};s.type="text/javascript";s.async=true;s.src="https://mod.calltouch.ru/init.js?id="+cId;if(w.opera=="[object Opera]"){d.addEventListener("DOMContentLoaded",i,false)}else{i()}}})(window,document,"ct",["s6kj2u59","khwofvym"]);
</script>
		<?php
	}
?>
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="https://analytics.yug-avto.ru/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '1']);
	_paq.push(['addTracker', u+'piwik.php', 4]);
	var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>

<!-- integration piwik + calltouch -->
<script type="text/javascript">
	var ct_max_wait = 100;
	var ct_wait_attr = setInterval(function() {
		ct_max_wait--; if (!ct_max_wait){ clearInterval(ct_wait_attr); }
		try {
			if (!!window.ct && !!window.ct_set_attrs && !!window.call_value) {
				if (!!window.Matomo && !!window.Matomo.getAsyncTrackers){
					[].forEach.call(window.Matomo.getAsyncTrackers(), function(el){
						if (el.getSiteId()=='1'){
							ct('set_attrs', '{"piwik_id":"'+el.getVisitorId()+'"}');
							clearInterval(ct_wait_attr);
						}
					}); 
				}
			}
		} catch(e) { console.log(e); }
	}, 200);
</script>
<!-- /integration piwik + calltouch -->

<script src="https://apps.yug-avto.ru/API/get/script/?token=42ab28dc2140261c218ba2c80dc4903c" charset="utf-8"></script>
