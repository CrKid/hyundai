<?
	$arRes = [];
	
	$arProps = ['FUTURES', 'FUTURE_STAR'];
	foreach ( $arProps as $prop ) {
		
		$rs = CIBlockElement::GetProperty(15, 22017, [], ['CODE'=>$prop]);
		while ($ob = $rs->GetNext()) ( $ob['MULTIPLE'] == 'Y' ) ? $arRes[$prop][] = $ob : $arRes[$prop] = $ob;
	}
?>

<div class="container futures my-5">
  <div class="row">
    <div class="col-md-12"><h2 class="text-center mb-5 title">Наши преимущества</h2></div>
  </div>
  <? $arS = [0, 5, 10, 15]; $arE = [4, 9, 14, 19]; ?>
  <? foreach ( $arRes['FUTURES'] as $k => $i ) { ?>
    <? if ( in_array($k, $arS) ) { ?><div class="row my-3"><? } ?>
      <div class="col">
        <div class="future-title"><?=$k+1?></div>
        <p class="my-3">
          <? if ( (int)$arRes['FUTURE_STAR']['VALUE'] == $k+1 ) { ?>
          <svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-Star5"></use></svg> 
          <? } ?>
          <?=$i['VALUE']?>
        </p>
      </div>
    <? if ( in_array($k, $arE) ) { ?></div><? } ?>
  <? } ?>
</div>