<div class="remodal" data-remodal-id="offer">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Получить максимальную выгоду</h3><h2 role="Car"></h2></div>
  </div>
  <form data-event="special">
    <input type="hidden" name="Form" value="Получить максимальную выгоду" />
    <input type="hidden" name="Section" value="12" />
    <input type="hidden" name="Car" value="" />
    <div class="row text-left">
      <div class="col-md-12">
        <div class="form-group">
          <input type="text" class="form-control" name="Name" placeholder="ФИО *" required />
        </div>
        <div class="form-group">
          <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
        </div>
        <div class="form-group">
          <select class="form-control" name="DC" required>
            <option disabled selected>Дилерский центр *</option>
            <? foreach ( $GLOBALS['SETTINGS']['DC'] as $i ) { ?>
            <option value="<?=$i['NAME']?>" <?=((count($GLOBALS['SETTINGS']['DC'])==1)?'selected':'')?>><?=$i['NAME']?></option>
            <? } ?>
          </select>
        </div>
        <div class="form-group my-3">
          <a href="#" class="but-darkblue btn-block p-2 text-center" role="Send">Отправить</a>
        </div>
        <div class="form-group">
          <p><small>Поля, отмеченные *, обязательны для заполнения.<br />Сообщая данные сведения, я даю согласие на обработку персональных данных и рекламную коммуникацию.</small></p>
        </div>
      </div>
    </div>
  </form>
  <div class="alert alert-dismissible alert-success">
    <strong>Спасибо за вашу заявку!</strong> Мы свяжемся с Вами в ближайшее время.
  </div>
  <div class="alert alert-dismissible alert-danger">
    <strong>Ой, что-то пошло не так!</strong> Повторите попытку позднее.
  </div>
</div>

<div class="remodal" data-remodal-id="Selection">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Оставить заявку на подбор</h3><h2 role="Car"></h2></div>
  </div>
  <form data-event="special">
    <input type="hidden" name="Form" value="Оставить заявку на подбор" />
    <input type="hidden" name="Section" value="16" />
    <input type="hidden" name="Car" value="" />
    <div class="row text-left">
      <div class="col-md-12">
        <div class="form-group">
          <input type="text" class="form-control" name="Name" placeholder="ФИО *" required />
        </div>
        <div class="form-group">
          <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
        </div>
        <div class="form-group">
          <select class="form-control" name="DC" required>
            <option disabled selected>Дилерский центр *</option>
            <? foreach ( $GLOBALS['SETTINGS']['DC'] as $i ) { ?>
            <option value="<?=$i['NAME']?>" <?=((count($GLOBALS['SETTINGS']['DC'])==1)?'selected':'')?>><?=$i['NAME']?></option>
            <? } ?>
          </select>
        </div>
        <div class="form-group my-3">
          <a href="#" class="but-darkblue btn-block p-2 text-center" role="Send">Отправить</a>
        </div>
        <div class="form-group">
          <p><small>Поля, отмеченные *, обязательны для заполнения.<br />Сообщая данные сведения, я даю согласие на обработку персональных данных и рекламную коммуникацию.</small></p>
        </div>
      </div>
    </div>
  </form>
  <div class="alert alert-dismissible alert-success">
    <strong>Спасибо за вашу заявку!</strong> Мы свяжемся с Вами в ближайшее время.
  </div>
  <div class="alert alert-dismissible alert-danger">
    <strong>Ой, что-то пошло не так!</strong> Повторите попытку позднее.
  </div>
</div>

<div class="remodal" data-remodal-id="testdrive">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Записаться на тест-драйв</h3></div>
  </div>
  <form data-event="test-drive">
    <input type="hidden" name="Form" value="Записаться на тест-драйв" />
    <input type="hidden" name="Section" value="10" />
    <div class="row text-left">
      <div class="col-md-12">
        <div class="form-group">
          <select class="form-control" name="Car" required>
            <option disabled selected>Выберите модель *</option>
            <? foreach ( $GLOBALS['SETTINGS']['TEST_DRIVE'] as $i ) { ?>
            <option value="<?=$i['PROPERTY_RU_NAME_VALUE']?>"><?=$i['PROPERTY_RU_NAME_VALUE']?></option>
            <? } ?>
          </select>
        </div>
        <div class="form-group">
          <input type="text" class="form-control" name="Name" placeholder="ФИО *" required />
        </div>
        <div class="form-group">
          <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
        </div>
        <div class="form-group">
          <select class="form-control" name="DC" required>
            <option disabled selected>Дилерский центр *</option>
            <? foreach ( $GLOBALS['SETTINGS']['DC'] as $i ) { ?>
            <option value="<?=$i['NAME']?>" <?=((count($GLOBALS['SETTINGS']['DC'])==1)?'selected':'')?>><?=$i['NAME']?></option>
            <? } ?>
          </select>
        </div>
        <div class="form-group my-3">
          <a href="#" class="but-darkblue btn-block p-2 text-center" role="Send">Отправить</a>
        </div>
        <div class="form-group">
          <p><small>Поля, отмеченные *, обязательны для заполнения.<br />Сообщая данные сведения, я даю согласие на обработку персональных данных и рекламную коммуникацию.</small></p>
        </div>
      </div>
    </div>
  </form>
  <div class="alert alert-dismissible alert-success">
    <strong>Спасибо за вашу заявку!</strong> Мы свяжемся с Вами в ближайшее время.
  </div>
  <div class="alert alert-dismissible alert-danger">
    <strong>Ой, что-то пошло не так!</strong> Повторите попытку позднее.
  </div>
</div>

<div class="remodal col2" data-remodal-id="service">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Скидка 10% за онлайн запись на сервис</h3></div>
  </div>
  <form data-event="service">
    <input type="hidden" name="Form" value="Скидка 10% за онлайн запись на сервис" />
    <input type="hidden" name="Section" value="11" />
    <div class="row text-left">
      <div class="col-md-6">
        <div class="form-group">
          <select class="form-control" name="Car" required>
            <option disabled selected>Выберите модель *</option>
            <? foreach ( $GLOBALS['SETTINGS']['SERVICE'] as $i ) { ?>
            <option value="<?=$i?>"><?=$i?></option>
            <? } ?>
          </select>
        </div>
        <div class="form-group">
          <input type="number" class="form-control" name="Year" placeholder="Год выпуска *" required />
        </div>
        <div class="form-group">
          <input type="number" class="form-control" name="Milleage" placeholder="Пробег *" required />
        </div>
        <div class="form-group">
          <select class="form-control" name="Work" required>
            <option value="Техническое обслуживание">Техническое обслуживание</option>
            <option value="Сервисное обслуживание">Сервисное обслуживание</option>
          </select>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <input type="text" class="form-control" name="Name" placeholder="ФИО *" required />
        </div>
        <div class="form-group">
          <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
        </div>
        <div class="form-group">
          <input type="email" class="form-control" name="Email" placeholder="Email" />
        </div>
        <div class="form-group">
          <select class="form-control" name="DC" required>
            <option disabled selected>Дилерский центр *</option>
            <? foreach ( $GLOBALS['SETTINGS']['DC'] as $i ) { ?>
            <option value="<?=$i['NAME']?>" <?=((count($GLOBALS['SETTINGS']['DC'])==1)?'selected':'')?>><?=$i['NAME']?></option>
            <? } ?>
          </select>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <textarea class="form-control" rows="5" name="Comment" placeholder="Ваш вопрос"></textarea>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group my-3">
          <a href="#" class="but-darkblue btn-block p-2 text-center" role="Send">Оставить заявку</a>
        </div>
        <div class="form-group">
          <p><small>Поля, отмеченные *, обязательны для заполнения.<br />Сообщая данные сведения, я даю согласие на обработку персональных данных и рекламную коммуникацию.</small></p>
        </div>
      </div>
    </div>
  </form>
  <div class="alert alert-dismissible alert-success">
    <strong>Спасибо за вашу заявку!</strong> Мы свяжемся с Вами в ближайшее время.
  </div>
  <div class="alert alert-dismissible alert-danger">
    <strong>Ой, что-то пошло не так!</strong> Повторите попытку позднее.
  </div>
</div>

<div class="remodal" data-remodal-id="tradein">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Спецусловия по Trade In</h3><h2 role="Car"></h2></div>
  </div>
  <form data-event="sell-car">
    <input type="hidden" name="Form" value="Спецусловия по Trade In" />
    <input type="hidden" name="Section" value="14" />
    <div class="row text-left">
      <div class="col-md-12">
        <div class="form-group">
          <input type="text" class="form-control" name="Name" placeholder="ФИО *" required />
        </div>
        <div class="form-group">
          <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
        </div>
        <div class="form-group">
          <select class="form-control" name="DC" required>
            <option disabled selected>Дилерский центр *</option>
            <? foreach ( $GLOBALS['SETTINGS']['DC'] as $i ) { ?>
            <option value="<?=$i['NAME']?>" <?=((count($GLOBALS['SETTINGS']['DC'])==1)?'selected':'')?>><?=$i['NAME']?></option>
            <? } ?>
          </select>
        </div>
        <div class="form-group my-3">
          <a href="#" class="but-darkblue btn-block p-2 text-center" role="Send">Оценить свой автомобиль</a>
        </div>
        <div class="form-group">
          <p><small>Поля, отмеченные *, обязательны для заполнения.<br />Сообщая данные сведения, я даю согласие на обработку персональных данных и рекламную коммуникацию.</small></p>
        </div>
      </div>
    </div>
  </form>
  <div class="alert alert-dismissible alert-success">
    <strong>Спасибо за вашу заявку!</strong> Мы свяжемся с Вами в ближайшее время.
  </div>
  <div class="alert alert-dismissible alert-danger">
    <strong>Ой, что-то пошло не так!</strong> Повторите попытку позднее.
  </div>
</div>

<div class="remodal" data-remodal-id="installment">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Спецусловия по рассрочке</h3><h2 role="Car"></h2></div>
  </div>
  <form data-event="credit">
    <input type="hidden" name="Form" value="Спецусловия по рассрочке" />
    <input type="hidden" name="Section" value="18" />
    <div class="row text-left">
      <div class="col-md-12">
        <div class="form-group">
          <input type="text" class="form-control" name="Name" placeholder="ФИО *" required />
        </div>
        <div class="form-group">
          <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
        </div>
        <div class="form-group">
          <select class="form-control" name="DC" required>
            <option disabled selected>Дилерский центр *</option>
            <? foreach ( $GLOBALS['SETTINGS']['DC'] as $i ) { ?>
            <option value="<?=$i['NAME']?>" <?=((count($GLOBALS['SETTINGS']['DC'])==1)?'selected':'')?>><?=$i['NAME']?></option>
            <? } ?>
          </select>
        </div>
        <div class="form-group my-3">
          <a href="#" class="but-darkblue btn-block p-2 text-center" role="Send">Оценить свой автомобиль</a>
        </div>
        <div class="form-group">
          <p><small>Поля, отмеченные *, обязательны для заполнения.<br />Сообщая данные сведения, я даю согласие на обработку персональных данных и рекламную коммуникацию.</small></p>
        </div>
      </div>
    </div>
  </form>
  <div class="alert alert-dismissible alert-success">
    <strong>Спасибо за вашу заявку!</strong> Мы свяжемся с Вами в ближайшее время.
  </div>
  <div class="alert alert-dismissible alert-danger">
    <strong>Ой, что-то пошло не так!</strong> Повторите попытку позднее.
  </div>
</div>
<div class="remodal" data-remodal-id="start">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Спецусловия по программе СТАРТ</h3><h2 role="Car"></h2></div>
  </div>
  <form data-event="special">
    <input type="hidden" name="Form" value="Спецусловия по программе СТАРТ" />
    <input type="hidden" name="Section" value="19" />
    <div class="row text-left">
      <div class="col-md-12">
        <div class="form-group">
          <input type="text" class="form-control" name="Name" placeholder="ФИО *" required />
        </div>
        <div class="form-group">
          <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
        </div>
        <div class="form-group">
          <select class="form-control" name="DC" required>
            <option disabled selected>Дилерский центр *</option>
            <? foreach ( $GLOBALS['SETTINGS']['DC'] as $i ) { ?>
            <option value="<?=$i['NAME']?>" <?=((count($GLOBALS['SETTINGS']['DC'])==1)?'selected':'')?>><?=$i['NAME']?></option>
            <? } ?>
          </select>
        </div>
        <div class="form-group my-3">
          <a href="#" class="but-darkblue btn-block p-2 text-center" role="Send">Оценить свой автомобиль</a>
        </div>
        <div class="form-group">
          <p><small>Поля, отмеченные *, обязательны для заполнения.<br />Сообщая данные сведения, я даю согласие на обработку персональных данных и рекламную коммуникацию.</small></p>
        </div>
      </div>
    </div>
  </form>
  <div class="alert alert-dismissible alert-success">
    <strong>Спасибо за вашу заявку!</strong> Мы свяжемся с Вами в ближайшее время.
  </div>
  <div class="alert alert-dismissible alert-danger">
    <strong>Ой, что-то пошло не так!</strong> Повторите попытку позднее.
  </div>
</div>
<div class="remodal" data-remodal-id="hansel">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Купить с первым взносом 0%</h3><h2 role="Car"></h2></div>
  </div>
  <form data-event="credit">
    <input type="hidden" name="Form" value="Купить с первым взносом 0%" />
    <input type="hidden" name="Section" value="20" />
    <div class="row text-left">
      <div class="col-md-12">
        <div class="form-group">
          <input type="text" class="form-control" name="Name" placeholder="ФИО *" required />
        </div>
        <div class="form-group">
          <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
        </div>
        <div class="form-group">
          <select class="form-control" name="DC" required>
            <option disabled selected>Дилерский центр *</option>
            <? foreach ( $GLOBALS['SETTINGS']['DC'] as $i ) { ?>
            <option value="<?=$i['NAME']?>" <?=((count($GLOBALS['SETTINGS']['DC'])==1)?'selected':'')?>><?=$i['NAME']?></option>
            <? } ?>
          </select>
        </div>
        <div class="form-group my-3">
          <a href="#" class="but-darkblue btn-block p-2 text-center" role="Send">Оценить свой автомобиль</a>
        </div>
        <div class="form-group">
          <p><small>Поля, отмеченные *, обязательны для заполнения.<br />Сообщая данные сведения, я даю согласие на обработку персональных данных и рекламную коммуникацию.</small></p>
        </div>
      </div>
    </div>
  </form>
  <div class="alert alert-dismissible alert-success">
    <strong>Спасибо за вашу заявку!</strong> Мы свяжемся с Вами в ближайшее время.
  </div>
  <div class="alert alert-dismissible alert-danger">
    <strong>Ой, что-то пошло не так!</strong> Повторите попытку позднее.
  </div>
</div>
