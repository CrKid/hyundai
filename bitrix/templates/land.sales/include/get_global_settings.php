<?	
	// Телефон
	$GLOBALS['SETTINGS']['PHONE'] = ['+7 (861) 210-44-00', '+7 (861) 730-11-01'];
	
	$arFilter = ['IBLOCK_ID'=>5, 'ACTIVE_DATE'=>'Y', 'ACTIVE'=>'Y'];
	$arSelect = ['ID', 'NAME', 'PROPERTY_ADDRESS', 'PROPERTY_PHONE'];
	
	$res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
	while ($ob = $res->GetNextElement()) $GLOBALS['SETTINGS']['DC'][] = $ob->GetFields();
	
	$arFilter = ['IBLOCK_ID'=>15, 'ID'=>22017, 'ACTIVE_DATE'=>'Y', 'ACTIVE'=>'Y'];
	$arSelect = ['ID', 'NAME', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'DETAIL_TEXT', 'DETAIL_PICTURE', 'PROPERTY_BANNER_BUTTON_TEXT', 'PROPERTY_BANNER_BUTTON_COLOR', 'PROPERTY_COUNTDOWN_USE', 'PROPERTY_COUNTDOWN_DATETIME', 'PROPERTY_COUNTDOWN_TEXT', 'PROPERTY_BANNER_PC_KRD', 'PROPERTY_BANNER_PC_NVR', 'PROPERTY_BANNER_M_KRD', 'PROPERTY_BANNER_M_NVR', 'PROPERTY_BANNER_BUTTON_TEXT_KRD', 'PROPERTY_BANNER_BUTTON_TEXT_NVR', 'PROPERTY_CALLTOUCH_KRD', 'PROPERTY_CALLTOUCH_NVR'];
	
	$res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
	while ($ob = $res->GetNextElement()) $GLOBALS['SETTINGS']['CONTENT'] = $ob->GetFields();
	
	if ( $GLOBALS['SETTINGS']['CONTENT']['PROPERTY_COUNTDOWN_USE_VALUE'] ) $GLOBALS['SETTINGS']['CONTENT']['TIMER']['RAW'] = (strtotime( $GLOBALS['SETTINGS']['CONTENT']['PROPERTY_COUNTDOWN_DATETIME_VALUE'] ) - time()) > 0 ? strtotime( $GLOBALS['SETTINGS']['CONTENT']['PROPERTY_COUNTDOWN_DATETIME_VALUE'] ) - time() : false;
	
	if ( $GLOBALS['SETTINGS']['CONTENT']['TIMER']['RAW'] ) {
		
		$raw = $GLOBALS['SETTINGS']['CONTENT']['TIMER']['RAW'];
		
		$tmp['DAYS'] = intdiv($raw, 24*60*60);
		$tmp['HOURS'] = intdiv($raw-$tmp['DAYS']*24*60*60, (60*60));
		$tmp['MINUTS'] = intdiv($raw-$tmp['DAYS']*24*60*60-$tmp['HOURS']*60*60, 60);
		$tmp['SECONDS'] = $raw-$tmp['DAYS']*24*60*60-$tmp['HOURS']*60*60-$tmp['MINUTS']*60;
		
		$GLOBALS['SETTINGS']['CONTENT']['TIMER'] = array_merge($GLOBALS['SETTINGS']['CONTENT']['TIMER'], $tmp);
	}
	
	$res = CIBlockElement::GetProperty(15, 22017, 'sort', 'asc', ['CODE'=>'SERVICE_MODELS']);
	while ( $ob = $res->GetNext() ) $GLOBALS['SETTINGS']['SERVICE'][] = $ob['VALUE'];
	 
	$arSelEl = ['ID', 'IBLOCK_ID', 'NAME', 'CODE', 'PROPERTY_RU_NAME'];
	$arFilEl = ['IBLOCK_ID' => 3, 'ACTIVE_DATE' => 'Y', 'ACTIVE' => 'Y', '=PROPERTY_TESTDRIVE_VALUE' => 'ON'];
	$arOrder = ['SORT' => 'ASC'];
	
	$rsEl = CIBlockElement::GetList($arOrder, $arFilEl, false, false, $arSelEl);
	while ($arEl = $rsEl->GetNextElement()) $GLOBALS['SETTINGS']['TEST_DRIVE'][] = $arEl->getFields();
	
	// Разбор урла
	$arUrl = explode('/', $_SERVER['REQUEST_URI']);
	$GLOBALS['SETTINGS']['CITY'] = ( $arUrl[2] ) ?: false;
	
	if ( $GLOBALS['SETTINGS']['CITY'] ) {
		
		switch ( $GLOBALS['SETTINGS']['CITY'] ) {
			
			case 'krd':
				unset ( $GLOBALS['SETTINGS']['DC'][2] );
				break;
				
			case 'nvr':
				unset ( $GLOBALS['SETTINGS']['DC'][0], $GLOBALS['SETTINGS']['DC'][1] );
				break;
		}
	}
	
	if ( $arUrl[2] && $arUrl[2] != 'krd' && $arUrl[2] != 'nvr' ) header('Location: /404.php', 301);
	
//	YApp::sp( $GLOBALS['SETTINGS'], true );
?>
    