<?
	$arRes = [];
	
	$arProps = ['CREDIT_IMAGE', 'CREDIT_LIST', 'CREDIT_BANK'];
	foreach ( $arProps as $prop ) {
		
		$rs = CIBlockElement::GetProperty(15, 22017, [], ['CODE'=>$prop]);
		while ($ob = $rs->GetNext()) ( $ob['MULTIPLE'] == 'Y' ) ? $arRes[$prop][] = $ob : $arRes[$prop] = $ob;
	}
?>

<div class="container bg-lightgray credit" data-block="credit">
  <div class="row p-4">
    <div class="col-md-4 pr-4"><img class="img-fluid" src="<?=CFile::GetPath($arRes['CREDIT_IMAGE']['VALUE'])?>" /></div>
    <div class="col">
      <h2>Экспресс-кредит</h2>
      <ul class="p-0 mt-4">
        <div class="row">
          <? foreach ( $arRes['CREDIT_LIST'] as $i ) { ?>
          <li class="col-md-6 my-2"><?=$i['VALUE']?></li>
          <? } ?>
        </div>
      </ul>
      <form data-event="credit">
        <input type="hidden" name="Form" value="Экспресс-кредит" />
        <input type="hidden" name="Section" value="13" />
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <input type="text" class="form-control" name="Name" placeholder="Ваше имя *" required>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <select class="form-control" name="DC" required>
                <option disabled selected>Дилерский центр *</option>
                <? foreach ( $GLOBALS['SETTINGS']['DC'] as $i ) { ?>
                <option value="<?=$i['NAME']?>"><?=$i['NAME']?></option>
                <? } ?>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <a href="#" class="but-darkblue btn-block p-2 text-center" role="Send">Рассчитать выгодный кредит</a>
            </div>
          </div>
          <div class="col-md-12">
            <p><small>Поля, отмеченные *, обязательны для заполнения.<br />Отправляя заявку Вы соглашаетесь на обработку персональных данных и рекламные коммуникации.</small></p>
          </div>
        </div>
      </form>
      <div class="alert alert-dismissible alert-success">
        <strong>Спасибо за вашу заявку!</strong> Мы свяжемся с Вами в ближайшее время.
      </div>
      <div class="alert alert-dismissible alert-danger">
        <strong>Ой, что-то пошло не так!</strong> Повторите попытку позднее.
      </div>
    </div>
  </div>
</div>

<div class="container my-5">
  	<? $arS = [0, 5, 10, 15]; $arE = [4, 9, 14, 19]; ?>
    <? foreach ( $arRes['CREDIT_BANK'] as $k => $i ) { ?>
	  <? if ( in_array($k, $arS) ) { ?><div class="row my-3"><? } ?>
      <div class="col text-center"><img class="img-fluid" alt="<? $APPLICATION->ShowTitle(); ?>" src="<?=CFile::GetPath($i['VALUE'])?>" /></div>
      <? if ( in_array($k, $arE) ) { ?></div><? } ?>
    <? } ?>
</div>