<?
	$arRes = [];
	
	$arProps = ['MODEL_TITLE', 'CAROUSEL_MODEL', 'CAROUSEL_IMAGE', 'CAROUSEL_BG', 'CAROUCEL_PRICE', 'CAROUCEL_LINE', 'CAROUCEL_LINE2', 'CAROUSEL_BUTTON1', 'CAROUSEL_BUTTON1_COLOR', 'CAROUSEL_BUTTON2', 'CAROUSEL_BUTTON2_COLOR'];
	foreach ( $arProps as $prop ) {
		
		$rs = CIBlockElement::GetProperty(15, 22017, [], ['CODE'=>$prop]);
		while ($ob = $rs->GetNext()) ( $ob['MULTIPLE'] == 'Y' ) ? $arRes[$prop][] = $ob : $arRes[$prop] = $ob;
	}
	
	foreach ( $arRes['CAROUSEL_MODEL'] as $k => $i ) {
		
		$arF = ['IBLOCK_ID'=>3, 'ID'=>$i];
		$arS = ['ID', 'CODE', 'PROPERTY_RU_NAME'];
		
		$arRes['MODELS'][] = CIBlockElement::GetList([], $arF, false, false, $arS)->GetNextElement()->GetFields();
	}
	
	
//	 YApp::sp( $arRes['MODEL_TITLE'], true );

?>
<div class="container my-2" data-block="offers">
  <div class="row py-5">
  	<div class="col-2 text-center prev-pic" style="display: block;">
      <h3 class="text-left pc"><?=$arRes['MODEL_TITLE'][count($arRes['MODEL_TITLE'])-1]['VALUE']?></h3>
      <img class="w-100 pc" alt="<?=$arRes['MODEL_TITLE'][count($arRes['MODEL_TITLE'])-1]['VALUE']?>" src="<?=CFile::GetPath($arRes['CAROUSEL_IMAGE'][count($arRes['CAROUSEL_IMAGE'])-1]['VALUE'])?>" />
      <a class="my-3" href="#" role="prev" data-slide="<?=count($arRes['MODEL_TITLE'])-1?>"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-ArrowLeft"></use></svg></a>
    </div>
    <div class="col swiper-container">
      <div class="swiper-bg" style="background-image: url(<?=CFile::GetPath($arRes['CAROUSEL_BG'][0]['VALUE'])?>);"></div>
      <div class="swiper-wrapper">
        <? foreach ( $arRes['MODEL_TITLE'] as $k => $i ) { ?>
      	<div class="swiper-slide text-center mt-2 pt-2" data-slide="<?=$k?>" data-image="<?=CFile::GetPath($arRes['CAROUSEL_IMAGE'][$k]['VALUE'])?>" data-bg="<?=CFile::GetPath($arRes['CAROUSEL_BG'][$k]['VALUE'])?>" data-name="<?=$i['VALUE']?>" data-credit="<span><?=$arRes['CAROUSEL_BUTTON2'][$k]['DESCRIPTION']?></span><br /><span>от</span> <?=number_format((int)$arRes['CAROUSEL_BUTTON2'][$k]['VALUE'], 0, '.', ' ')?><br /><span>₽/мес.</span>" data-butcredit="За <?=number_format((int)$arRes['CAROUSEL_BUTTON2'][$k]['VALUE'], 0, '.', ' ')?> ₽/мес.">
          
          <h2 class="col-9 ml-5 pl-5 pc"><?=$arModelJS['NAME'] = $i['~VALUE']?></h2>
          <h4 class="col-9 ml-5 pl-5 pc"><?=$arRes['CAROUCEL_PRICE'][$k]['DESCRIPTION']?>  <?=$arRes['CAROUCEL_PRICE'][$k]['VALUE']?></h4>
          <? if ( $arRes['CAROUCEL_LINE'][$k]['VALUE'] ) { ?>
          <h4 class="col-7 ml-5 pl-5 pc"><?=$arRes['CAROUCEL_LINE'][$k]['VALUE']?></h4>
          <? } ?>
          <? if ( $arRes['CAROUCEL_LINE2'][$k]['VALUE'] ) { ?>
          <h4 class="col-7 ml-5 pl-5 pc"><?=$arRes['CAROUCEL_LINE2'][$k]['VALUE']?></h4>
          <? } ?>
          
          <h2 class="col pl-1 mob"><?=$arModelJS['NAME'] = $i['~VALUE']?></h2>
          <h4 class="col pl-1 mob"><?=$arRes['CAROUCEL_PRICE'][$k]['DESCRIPTION']?>  <?=$arRes['CAROUCEL_PRICE'][$k]['VALUE']?></h4>
          <? if ( $arRes['CAROUCEL_LINE'][$k]['VALUE'] ) { ?>
          <h4 class="col pl-1 mob"><?=$arRes['CAROUCEL_LINE'][$k]['VALUE']?></h4>
          <? } ?>
          <? if ( $arRes['CAROUCEL_LINE2'][$k]['VALUE'] ) { ?>
          <h4 class="col pl-1 mob"><?=$arRes['CAROUCEL_LINE2'][$k]['VALUE']?></h4>
          <? } ?>
          
          <img alt="<?=$arModelJS['NAME']?>" src="<?=$arModelJS['PIC'] = CFile::GetPath($arRes['CAROUSEL_IMAGE'][$k]['VALUE'])?>" />
          <? if ( $arRes['CAROUSEL_BUTTON1'][$k]['VALUE'] ) { ?>
          <div class="my-5 py-5"></div>
          <?php /*
          <a href="#installment" class="button but-white px-4 py-2 mb-2 text-center" role="Credit" data-car="Hyundai <?=$arRes['MODEL_TITLE'][$k]['VALUE']?>">В рассрочку 0%</a><br />
		  */ ?>
          <a href="#start" class="button but-white px-4 py-2 mb-2 text-center" role="Credit" data-car="Hyundai <?=$arRes['MODEL_TITLE'][$k]['VALUE']?>">За <?=number_format((int)$arRes['CAROUSEL_BUTTON2'][$k]['VALUE'], 0, '.', ' ')?> ₽/мес.</a><br />
          <a href="#hansel" class="button but-white px-4 py-2 mb-2 text-center" role="Credit" data-car="Hyundai <?=$arRes['MODEL_TITLE'][$k]['VALUE']?>">Первый взнос 0%</a><br />
          <a href="#tradein" class="button but-white px-4 py-2 mb-2 text-center">Выгодный Trade-In</a><br />
          <a href="#Selection" class="button but-white px-4 py-2 mb-2 text-center" role="Selection" data-car="Hyundai <?=$arRes['MODEL_TITLE'][$k]['VALUE']?>">Оставить заявку на подбор</a><br />
          <a href="<?=$arRes['CAROUSEL_BUTTON1'][$k]['DESCRIPTION']?>" class="button pc" role="Offer" style="<?=(($arRes['CAROUSEL_BUTTON1_COLOR']['VALUE'])?'background: '.$arRes['CAROUSEL_BUTTON1_COLOR']['VALUE'].';':'')?>" data-car="Hyundai <?=$arModelJS['NAME']?>"><?=$arRes['CAROUSEL_BUTTON1'][$k]['VALUE']?></a>
          <? } ?>
          <? /* if ( $arRes['CAROUSEL_BUTTON2'][$k]['VALUE'] ) { ?>
          <a href="#credit" class="pc" role="scroll" data-scroll="credit" style="<?=(($arRes['CAROUSEL_BUTTON2_COLOR']['VALUE'])?'background: '.$arRes['CAROUSEL_BUTTON2_COLOR']['VALUE'].';':'')?>" data-car="Hyundai <?=$arModelJS['NAME']?>">
		    <span><?=$arRes['CAROUSEL_BUTTON2'][$k]['DESCRIPTION']?></span><br />
			<span>от</span> <?=number_format((int)$arRes['CAROUSEL_BUTTON2'][$k]['VALUE'], 0, '.', ' ')?><br />
            <span>₽/мес.</span>
          </a>
          <? } */?>
        </div>
        <? $arModelPics[] = $arModelJS; ?>
        <? } ?>
      </div> 
    </div>
    <div class="col-2 text-center next-pic" style="display: block;">
      <h3 class="text-left pc"><?=$arRes['MODEL_TITLE'][1]['VALUE']?></h3>
      <img class="w-100 pc" alt="<?=$arRes['MODEL_TITLE'][1]['VALUE']?>" src="<?=CFile::GetPath($arRes['CAROUSEL_IMAGE'][1]['VALUE'])?>" />
      <a class="my-3" href="#" role="next" data-slide="1"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-ArrowRight"></use></svg></a>
    </div>
  </div>
  
  <? if ( $arRes['CAROUSEL_BUTTON1'][0]['VALUE'] ) { ?>
  <div class="row mob">
    <div class="col">
      <?php /*
      <a href="#installment" id="offer" class="button but-white px-4 py-2 mb-2 text-center btn-block" role="Credit" data-car="Hyundai <?=$arRes['MODEL_TITLE'][0]['VALUE']?>">В рассрочку 0%</a>
	  */ ?>
      <a href="#start" id="offer" class="button but-white px-4 py-2 mb-2 text-center btn-block" role="Credit" for="Credit" data-car="Hyundai <?=$arRes['MODEL_TITLE'][0]['VALUE']?>">За <?=number_format((int)$arRes['CAROUSEL_BUTTON2'][0]['VALUE'], 0, '.', ' ')?> ₽/мес.</a>
      <a href="#hansel" id="offer" class="button but-white px-4 py-2 mb-2 text-center btn-block" role="Credit" data-car="Hyundai <?=$arRes['MODEL_TITLE'][0]['VALUE']?>">Первый взнос 0%</a>
      <a href="#tradein" class="button but-white px-4 py-2 mb-2 text-center btn-block">Выгодный Trade-In</a>
      <a href="#Selection" class="button but-white px-4 py-2 mb-2 text-center btn-block">Оставить заявку на подбор</a>
      <a href="<?=$arRes['CAROUSEL_BUTTON1'][0]['DESCRIPTION']?>" class="px-4 py-2 text-center btn-block but-red" role="Offer" id="offer" style="<?=(($arRes['CAROUSEL_BUTTON1_COLOR']['VALUE'])?'background: '.$arRes['CAROUSEL_BUTTON1_COLOR']['VALUE'].';':'')?>" data-car="Hyundai <?=$arRes['MODEL_TITLE'][0]['PROPERTY_RU_NAME_VALUE']?>"><?=$arRes['CAROUSEL_BUTTON1'][$k]['VALUE']?></a>
    </div>
  </div>
  <? } ?>
  <? /* if ( $arRes['CAROUSEL_BUTTON2'][0]['VALUE'] ) { ?>
  <div class="row mob position-relative">
    <div class="col-2"></div>
    <div class="col">
      <a href="#credit" class="but-petal" role="scroll" data-scroll="credit" id="offer" style="<?=(($arRes['CAROUSEL_BUTTON2_COLOR']['VALUE'])?'background: '.$arRes['CAROUSEL_BUTTON2_COLOR']['VALUE'].';':'')?>" data-car="Hyundai <?=$arRes['MODEL_TITLE'][0]['PROPERTY_RU_NAME_VALUE']?>">
	    <span><?=$arRes['CAROUSEL_BUTTON2'][0]['DESCRIPTION']?></span><br />
        <span>от</span> <?=number_format((int)$arRes['CAROUSEL_BUTTON2'][0]['VALUE'], 0, '.', ' ')?><br />
        <span>₽/мес.</span>
      </a>
    </div>
    <div class="col-2"></div>
  </div>
  <? } */?>
</div>

<? /*
<div class="container mb-5">
  <div class="row w-100">
   
    <div class="col-md-12 text-center pc">
      <a href="#installment" id="offer" class="but-white px-4 py-2 text-center" role="Credit" data-car="Hyundai <?=$arRes['MODEL_TITLE'][0]['VALUE']?>">В рассрочку 0%</a>
	  <a href="#start" id="offer" class="but-white px-4 py-2 text-center" role="Credit" data-car="Hyundai <?=$arRes['MODEL_TITLE'][0]['VALUE']?>">За <?=number_format((int)$arRes['CAROUSEL_BUTTON2'][0]['VALUE'], 0, '.', ' ')?> ₽/мес.</a>
	  <a href="#hansel" id="offer" class="but-white px-4 py-2 text-center" role="Credit" data-car="Hyundai <?=$arRes['MODEL_TITLE'][0]['VALUE']?>">Первый взнос 0%</a>
	  <a href="#tradein" class="but-white px-4 py-2 text-center">Выгодный Trade-In</a>
	  <a href="#tour" class="but-white px-4 py-2 text-center">Путевка в Турцию в подарок</a>
    </div>
    
    <div class="col-md-12 pb-1 text-center mob"><a href="#installment" id="offer" class="but-white px-4 py-2 text-center" role="Credit" data-car="Hyundai <?=$arRes['MODEL_TITLE'][0]['VALUE']?>">В рассрочку 0%</a></div>
	<div class="col-md-12 pb-1 text-center mob"><a href="#start" id="offer" class="but-white px-4 py-2 text-center" role="Credit" data-car="Hyundai <?=$arRes['MODEL_TITLE'][0]['VALUE']?>">За <?=number_format((int)$arRes['CAROUSEL_BUTTON2'][0]['VALUE'], 0, '.', ' ')?> ₽/мес.</a></div>
	<div class="col-md-12 pb-1 text-center mob"><a href="#hansel" id="offer" class="but-white px-4 py-2 text-center" role="Credit" data-car="Hyundai <?=$arRes['MODEL_TITLE'][0]['VALUE']?>">Первый взнос 0%</a></div>
	<div class="col-md-12 pb-1 text-center mob"><a href="#tradein" class="but-white px-4 py-2 text-center">Выгодный Trade-In</a></div>
	<div class="col-md-12 text-center mob"><a href="#tour" class="but-white px-4 py-2 text-center">Путевка в Турцию в подарок</a></div>
    
  </div>
</div>
*/?>