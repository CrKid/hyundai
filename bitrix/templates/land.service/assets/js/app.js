'use strict';

Array.prototype.in_array = function(p_val) {
	for(var i = 0, l = this.length; i < l; i++)	{
		if(this[i] == p_val) {
			return true;
		}
	}
	return false;
}



$('input[type="phone"]').inputmask({'mask': '+7 (999) 999-99-99', showMaskOnHover: false});


$('a[role="Offer"], a[role="Credit"]').click( function() {
	
	var Car = ( typeof $(this).data('car') != 'undefined' ) ? $(this).data('car') : '';
	
	var w = $('div[data-remodal-id="offer"]');
	w.find('input[name="Car"]').val( Car );
	w.find('h2[role="Car"]').text( Car );
	
	w = $('div[data-remodal-id="credit"]');
	w.find('input[name="Car"]').val( Car );
	w.find('h2[role="Car"]').text( Car );
});

$('[role="scroll"]').click( function() {
	
	$('html, body').animate({ scrollTop: $('[data-block="' + $(this).data('scroll') + '"]').offset().top }, 500);
});

$('[type="email"]').blur( function() {
	
	if ( $(this).val() && !YApps.CheckEmail( $(this).val() ) ) {
		
		$(this).addClass('is-invalid'); 
		
	} else {
		
		$(this).removeClass('is-invalid'); 
	}
});

$('a[role="showActionForm"]').click( function() {
	
	$(this).parent().children('.card-form').fadeIn(300);
	return false;
});

$('a[role="closeActionForm"]').click( function() {
	
	$(this).parent().fadeOut(300);
	return false;
});


$('a[role="Send"]').click( function() {
	
	var Form = $(this).parents('form');
	
	var success = $(Form).parent().find('.alert-success');
	var error = $(Form).parent().find('.alert-danger');
	var Event = $(Form).data('event');
	
	YApps.SendData.AppName = 'Lands';
	YApps.SendData.EventCategory = 'Заполена форма';
	
	YApps.SendData.Flag = true;
	
	Form.find('input, select, textarea').each( function(i, e) {
		
		YApps.SendData[$(e).attr('name')] = $(e).val();
		$(e).removeClass('is-invalid');
		
		if ( $(e).attr('required') ) {
			
			if ( !$(e).val() ) {
				
				YApps.SendData.Flag = false;
				$(e).addClass('is-invalid');
			}
		}
		
		if ( $(e).is('[type="email"]') ) {
			
			if ( $(e).val() && !YApps.CheckEmail( $(e).val() ) ) {
				
				YApps.SendData.Flag = false;
				$(e).addClass('is-invalid');
			}
		}
	});
	
	if ( YApps.SendData.Flag ) {
		
		$.ajax({
			type: "POST",
			url: '/service-actions/ajax.php',  
			data: YApps.SendData,
			success: function(data){
				
				var res = JSON.parse( data );
				
				if ( res.status == 'success' ) {
					
					$(Form).slideUp(300);
					$(success).slideDown(300);
					$(error).hide();
					
					if ( typeof fbq != 'undefined' ) fbq('track', 'Lead');
					if (typeof yaCounter7783918 != 'undefined') yaCounter7783918.reachGoal('HyundaiLandind_send');
                    if (typeof Matomo != 'undefined') _paq.push(["trackEvent", 'Формы лэндинга', YApps.SendData.Form, 'Отправка']);
					
					var Layer = {
						'event': 'FormSubmission',
						'eventCategory': Event,
						'eventAction': 'submit',
						'eventLabel': YApps.SendData.Car || null
					};
					dataLayer.push( Layer );
					
					var d = ( YApps.SendData.DC == 'Hyundai г. Новороссийск' ) ? '20893' : '20892' ;
					var s = ( YApps.SendData.DC == 'Hyundai г. Новороссийск' ) ? window.call_value_5 : window.call_value_4 ;
					
					var CallTouchURL = 'https://api-node7.calltouch.ru/calls-service/RestAPI/requests/'+d+'/register/';
					CallTouchURL += '?subject=Формы лэндинга - '+YApps.SendData.Form;
					CallTouchURL += '&sessionId='+s;
					CallTouchURL += '&fio='+YApps.SendData.Name;
					CallTouchURL += '&phoneNumber='+YApps.SendData.Phone.replace(/[^\d;]/g, '');
					
					$.get( CallTouchURL );
					
					YApps.AppPushStat( YApps.SendData );
					
				} else {
					
					$(error).slideDown(300);
				}
			},
			error: function() {
				
				$(error).slideUp(300);
			}
		});
	}
	
	return false;
});