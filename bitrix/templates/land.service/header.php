<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); CModule::IncludeModule('iblock'); ?>
<? include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/get_global_settings.php'; ?> 
<!doctype html>
<html lang="ru">
  <head>
  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NG6KHHV');</script>
<!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Boretscy A">
    <title><? $APPLICATION->ShowTitle(); ?></title>

    <!-- Bootstrap core CSS -->
	<link href="<?=SITE_TEMPLATE_PATH?>/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal-default-theme.min.css" />
    
    <?// $APPLICATION->ShowHead()?>
    
    <link href="<?=SITE_TEMPLATE_PATH?>/assets/fonts/fonts.css?<?=md5_file($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/assets/fonts/fonts.css')?>" rel="stylesheet">
    <link href="<?=SITE_TEMPLATE_PATH?>/assets/css/app.css?<?=md5_file($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/assets/css/app.css')?>" rel="stylesheet">
    
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://hyundai.yug-avto.ru/"; />
    <meta property="og:title" content="<? $APPLICATION->ShowTitle(); ?>" />
    <meta property="og:description" content="<? $APPLICATION->ShowTitle(); ?>" />
    <meta property="og:image" content="http://hyundai.yug-avto.ru/bitrix/templates/main/assets/images/og_logo.jpg"  />
    
    <script src="<?=SITE_TEMPLATE_PATH?>/assets/js/init.js?<?=md5_file($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/assets/js/init.js');?>"></script>
    
    <!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '386535742186539');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=386535742186539&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

    
  </head>
  <body>
  	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NG6KHHV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	<? // $APPLICATION->ShowPanel();?>
    <? include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/_counters.php'; ?>
	
    <div class="top-row bg-lightgray">
      <div class="container">
        <div class="row py-3">
          <div class="col logo-hyundai-yugavto mob">
            <a href="/specials/"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-LogoHyundaiYugavto"></use></svg></a>
          </div>
          <div class="col text-right">
            <ul class="p-0 m-0">
              <li class="d-inline-block ml-4"><span class="mr-2 pc">Краснодар</span> <a href="tel:<?=YApp::formatPhoneIn($GLOBALS['SETTINGS']['PHONE'][0])?>"><?=YApp::formatPhoneOut($GLOBALS['SETTINGS']['PHONE'][0])?></a></li>
              <li class="d-inline-block ml-4"><span class="mr-2 pc">Новороссийск</span> <a href="tel:<?=YApp::formatPhoneIn($GLOBALS['SETTINGS']['PHONE'][1])?>"><?=YApp::formatPhoneOut($GLOBALS['SETTINGS']['PHONE'][1],true)?></a></li>
              <li class="d-inline-block ml-4 pc"><a href="#" role="scroll" data-scroll="contacts"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-CapretDown"></use></svg></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    
    <div class="logo-row pc">
      <div class="container">
        <div class="row py-4">
          <div class="col-6 col-md-6 logo-hyundai-yugavto">
            <a href="/specials/"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-LogoHyundaiYugavto"></use></svg></a>
          </div>
          <div class="col-6 col-md-6 text-right pc" style="display: block;">
          	<ul class="p-0 m-0">
              <li class="d-inline-block ml-4"><a href="#tradein"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-CarRefresh"></use></svg> Trade-in</a></li>
              <li class="d-inline-block ml-4"><a href="#service"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-Wrench"></use></svg> Сервис</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    
    <? include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/_banner.php'; ?>
    <? include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/_futures.php'; ?>
    <? include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/_specials.php'; ?>
    <? include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/_services.php'; ?>
    
    <div class="bg-lightgray py-5">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h3>Не нашли нужную услугу?</h3>
            <p>Позвоните нам по телефону <a href="tel:<?=YApp::formatPhoneIn($GLOBALS['SETTINGS']['PHONE'][0])?>"><?=YApp::formatPhoneOut($GLOBALS['SETTINGS']['PHONE'][0])?></a> и мы ответим на все вопросы!</p>
          </div>
          <div class="col-md-4"></div>
          <div class="col-md-4">
            <a href="#callback" class="but-red btn-block p-2 text-center">Заказать обратный звонок</a>
          </div>
          <div class="col-md-4"></div>
        </div>
      </div>
    </div>
    
    
    <div class="position-relative bg-darkblue" data-block="contacts">
      
      <div class="container">
        <div class="row">
          <div class="col p-4 contacts">
            <svg class="logo" xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-LogoYugavto"></use></svg>
            <h5 class="py-2">Официальный дилер Hyundai</h5>
            <ul class="p-0 my-3">
              <? foreach ( $GLOBALS['SETTINGS']['DC'] as $i ) { ?>
              <li class="py-2">
                <h5 class="py-2"><?=$i['NAME']?></h5>
                <p><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-Map"></use></svg> <?=$i['PROPERTY_ADDRESS_VALUE']?>
                <br />
                  <svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-Phone"></use></svg> 
                  <a href="tel:<?=YApp::formatPhoneIn($i['PROPERTY_PHONE_VALUE'])?>"><?=YApp::formatPhoneOut($i['PROPERTY_PHONE_VALUE'])?></a></p>
              </li>
              <? } ?>
            </ul>
          </div>
        </div>
      </div>
      
      <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3AbvYl8ki_R0aftJ8W1JqDbyRDz_w6TnaJ&amp;width=100%&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>
      
    </div>