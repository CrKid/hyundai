<?
	$arRes = [];
	
	
	$rs = CIBlockElement::GetProperty(15, 32231, [], ['CODE'=>'FUTURES']);
	while ($ob = $rs->GetNext()) $arRes[] = $ob;
?>

<div class="bg-darkblue py-5">
  <div class="container futures">
    <div class="row">
      <div class="col-md-12">
        <h2 class="text-center mb-5 title position-relative">
          Наши преимущества
          <div class="bborder"></div>
        </h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-2"></div>
      <? foreach ( $arRes as $item ) { ?>
      <div class="col-md-1 text-center">
        <svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-<?=$item['DESCRIPTION']?>"></use></svg> 
        <p class="mt-3" style="line-height: 1;"><small><?=$item['VALUE']?></small></p>
      </div>
      <? } // foreach ?>
      <div class="col-md-2"></div>
    </div>
  </div>
</div>