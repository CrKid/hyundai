<div class="remodal" data-remodal-id="offer">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Получить максимальную выгоду</h3><h2 role="Car"></h2></div>
  </div>
  <form data-event="special">
    <input type="hidden" name="Form" value="Получить максимальную выгоду" />
    <input type="hidden" name="Section" value="12" />
    <input type="hidden" name="Source" value="Лендинг сервиса" />
    <input type="hidden" name="Car" value="" />
    <div class="row text-left">
      <div class="col-md-12">
        <div class="form-group">
          <input type="text" class="form-control" name="Name" placeholder="ФИО *" required />
        </div>
        <div class="form-group">
          <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
        </div>
        <div class="form-group">
          <select class="form-control" name="DC" required>
            <option disabled selected>Дилерский центр *</option>
            <? foreach ( $GLOBALS['SETTINGS']['DC'] as $i ) { ?>
            <option value="<?=$i['NAME']?>"><?=$i['NAME']?></option>
            <? } ?>
          </select>
        </div>
        <div class="form-group my-3">
          <a href="#" class="but-darkblue btn-block p-2 text-center" role="Send">Получить лучшую цену</a>
        </div>
        <div class="form-group"><? include '_forms.personal.php'; ?></div>
      </div>
    </div>
  </form>
  <? include '_forms.result.php'; ?>
</div>

<div class="remodal" data-remodal-id="testdrive">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Записаться на тест-драйв</h3></div>
  </div>
  <form data-event="test-drive">
    <input type="hidden" name="Form" value="Записаться на тест-драйв" />
    <input type="hidden" name="Section" value="10" />
    <input type="hidden" name="Source" value="Лендинг сервиса" />
    <div class="row text-left">
      <div class="col-md-12">
        <div class="form-group">
          <select class="form-control" name="Car" required>
            <option disabled selected>Выберите модель *</option>
            <? foreach ( $GLOBALS['SETTINGS']['TEST_DRIVE'] as $i ) { ?>
            <option value="<?=$i['PROPERTY_RU_NAME_VALUE']?>"><?=$i['PROPERTY_RU_NAME_VALUE']?></option>
            <? } ?>
          </select>
        </div>
        <div class="form-group">
          <input type="text" class="form-control" name="Name" placeholder="ФИО *" required />
        </div>
        <div class="form-group">
          <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
        </div>
        <div class="form-group">
          <select class="form-control" name="DC" required>
            <option disabled selected>Дилерский центр *</option>
            <? foreach ( $GLOBALS['SETTINGS']['DC'] as $i ) { ?>
            <option value="<?=$i['NAME']?>"><?=$i['NAME']?></option>
            <? } ?>
          </select>
        </div>
        <div class="form-group my-3">
          <a href="#" class="but-darkblue btn-block p-2 text-center" role="Send">Отправить</a>
        </div>
        <div class="form-group"><? include '_forms.personal.php'; ?></div>
      </div>
    </div>
  </form>
  <? include '_forms.result.php'; ?>
</div>

<div class="remodal col2" data-remodal-id="service">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Запись на сервисное или техническое обслуживание</h3></div>
  </div>
  <form data-event="service">
    <input type="hidden" name="Form" value="Запись на сервисное или техническое обслуживание" />
    <input type="hidden" name="Section" value="11" />
    <input type="hidden" name="Source" value="Лендинг сервиса" />
    <div class="row text-left">
      <div class="col-md-6">
        <div class="form-group">
          <select class="form-control" name="Car" required>
            <option disabled selected>Выберите модель *</option>
            <? foreach ( $GLOBALS['SETTINGS']['SERVICE'] as $i ) { ?>
            <option value="<?=$i?>"><?=$i?></option>
            <? } ?>
          </select>
        </div>
        <div class="form-group">
          <input type="number" class="form-control" name="Year" placeholder="Год выпуска *" required />
        </div>
        <div class="form-group">
          <input type="number" class="form-control" name="Milleage" placeholder="Пробег *" required />
        </div>
        <div class="form-group">
          <select class="form-control" name="Work" required>
            <option value="Техническое обслуживание">Техническое обслуживание</option>
            <option value="Сервисное обслуживание">Сервисное обслуживание</option>
          </select>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <input type="text" class="form-control" name="Name" placeholder="ФИО *" required />
        </div>
        <div class="form-group">
          <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
        </div>
        <div class="form-group">
          <input type="email" class="form-control" name="Email" placeholder="Email" />
        </div>
        <div class="form-group">
          <select class="form-control" name="DC" required>
            <option disabled selected>Дилерский центр *</option>
            <? foreach ( $GLOBALS['SETTINGS']['DC'] as $i ) { ?>
            <option value="<?=$i['NAME']?>"><?=$i['NAME']?></option>
            <? } ?>
          </select>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <textarea class="form-control" rows="5" name="Comment" placeholder="Ваш вопрос"></textarea>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group my-3">
          <a href="#" class="but-darkblue btn-block p-2 text-center" role="Send">Оставить заявку</a>
        </div>
        <div class="form-group"><? include '_forms.personal.php'; ?></div>
      </div>
    </div>
  </form>
  <? include '_forms.result.php'; ?>
</div>

<div class="remodal" data-remodal-id="tradein">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Спецусловия по Trade In</h3></div>
  </div>
  <form data-event="sell-car">
    <input type="hidden" name="Form" value="Спецусловия по Trade In" />
    <input type="hidden" name="Section" value="14" />
    <input type="hidden" name="Source" value="Лендинг сервиса" />
    <div class="row text-left">
      <div class="col-md-12">
        <div class="form-group">
          <input type="text" class="form-control" name="Name" placeholder="ФИО *" required />
        </div>
        <div class="form-group">
          <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
        </div>
        <div class="form-group">
          <select class="form-control" name="DC" required>
            <option disabled selected>Дилерский центр *</option>
            <? foreach ( $GLOBALS['SETTINGS']['DC'] as $i ) { ?>
            <option value="<?=$i['NAME']?>"><?=$i['NAME']?></option>
            <? } ?>
          </select>
        </div>
        <div class="form-group my-3">
          <a href="#" class="but-darkblue btn-block p-2 text-center" role="Send">Оценить свой автомобиль</a>
        </div>
        <div class="form-group"><? include '_forms.personal.php'; ?></div>
      </div>
    </div>
  </form>
  <? include '_forms.result.php'; ?>
</div>

<div class="remodal" data-remodal-id="callback">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Заказать обратный звонок</h3></div>
  </div>
  <form data-event="callback">
    <input type="hidden" name="Form" value="Заказать обратный звонок" />
    <input type="hidden" name="Section" value="15" />
    <input type="hidden" name="Source" value="Лендинг сервиса" />
    <div class="row text-left">
      <div class="col-md-12">
        <div class="form-group">
          <input type="text" class="form-control" name="Name" placeholder="ФИО *" required />
        </div>
        <div class="form-group">
          <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
        </div>
        <div class="form-group">
          <select class="form-control" name="DC" required>
            <option disabled selected>Дилерский центр *</option>
            <? foreach ( $GLOBALS['SETTINGS']['DC'] as $i ) { ?>
            <option value="<?=$i['NAME']?>"><?=$i['NAME']?></option>
            <? } ?>
          </select>
        </div>
        <div class="form-group my-3">
          <a href="#" class="but-darkblue btn-block p-2 text-center" role="Send">Оставить заявку</a>
        </div>
        <div class="form-group"><? include '_forms.personal.php'; ?></div>
      </div>
    </div>
  </form>
  <? include '_forms.result.php'; ?>
</div>
