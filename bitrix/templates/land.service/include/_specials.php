<?
	$arOrder = ['timestamp_x'=>'desc'];
	$arFilter = ['IBLOCK_ID'=>9, 'SECTION_ID'=>6, 'ACTIVE'=>'Y', 'PROPERTY_USE_IN_LAND_VALUE'=>'Да'];
	$arSelect = ['ID', 'NAME', 'CODE', 'PREVIEW_PICTURE', 'PREVIEW_TEXT', 'PROPERTY_EXTERNAL_LINK'];
	
	$arRes = [];
	$rs = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
	while ( $ob = $rs->GetNextElement() ) $arRes[] = $ob->GetFields();
	
	YApp::sp( $arRes[0], true );
?>
<div class="container actions">
  <div class="row my-5">
    <div class="col-md-12 text-center">
      <h2 class="title">
        Акции сервисного центра Юг-Авто
        <div class="bborder"></div>
      </h2>
    </div>
  </div>
  <div class="row mb-5">
    <? foreach ( $arRes as $item ) { ?>
    <div class="col-md-6 my-3 text-center position-relative">
      <a href="#" role="showActionForm">
        <img class="w-100" src="<?=CFile::GetPath( $item['PREVIEW_PICTURE'] );?>" alt="<?=$item['NAME']?>" />
        <div class="bg-darkblue p-3"><?=$item['NAME']?> <svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-More"></use></svg></div>
      </a>
      <div class="card-form p-4 bg-darkblue h-100 position-absolute">
        <a href="#" role="closeActionForm"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-Close"></use></svg></a>
        <h5 class="text-left"><?=$item['NAME']?></h5>
        <p class="text-left"><?=$item['PREVIEW_TEXT']?></p>
        <p class="text-left"><a class="hc-white" href="<?=$item['PROPERTY_EXTERNAL_LINK_VALUE']?>" target="_blank">Подробнее &rarr;</a></p>
        <form class="mt-4 pt-4" data-event="special">
          <input type="hidden" name="Form" value="Заказать обратный звонок" />
          <input type="hidden" name="Section" value="15" />
          <input type="hidden" name="Source" value="Акция: <?=$item['NAME']?>" />
          <div class="row text-left">
            <div class="col-md-12">
              <div class="form-group">
                <input type="text" class="form-control" name="Name" placeholder="ФИО *" required />
              </div>
              <div class="form-group">
                <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
              </div>
              <div class="form-group">
                <select class="form-control" name="DC" required>
                  <option disabled selected>Дилерский центр *</option>
                  <? foreach ( $GLOBALS['SETTINGS']['DC'] as $i ) { ?>
                  <option value="<?=$i['NAME']?>"><?=$i['NAME']?></option>
                  <? } ?>
                </select>
              </div>
              <div class="form-group my-3">
                <a href="#" class="but-red btn-block p-2 text-center" role="Send">Оставить заявку</a>
              </div>
              <div class="form-group"><? include '_forms.personal.php'; ?></div>
            </div>
          </div>
        </form>
        <? include '_forms.result.php'; ?>
      </div>
    </div>
    <? } // foreach ?>
  </div>
</div>