<?
	$arOrder = ['order'=>'asc'];
	$arFilter = ['IBLOCK_ID'=>18, 'ACTIVE'=>'Y'];
	$arSelect = ['ID', 'NAME', 'PROPERTY_ICON'];
	
	$arRes = [];
	$rs = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
	while ( $ob = $rs->GetNextElement() ) $arRes[] = $ob->GetFields();
?>
<div class="container services">
  <div class="row my-5">
    <div class="col-md-12 text-center">
      <h2 class="title">
        Наши услуги
        <div class="bborder"></div>
      </h2>
    </div>
  </div>
  <div class="row mb-5">
    <? foreach ( $arRes as $item ) { ?>
    <div class="col-md-4 my-3 pl-4">
      <svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-<?=$item['PROPERTY_ICON_VALUE']?>"></use></svg> 
      <?=$item['NAME']?>
    </div>
    <? } // foreach ?>
  </div>
</div>