	
	<div class="banner pb-5">
      <div class="container">
        <div class="row pb-4 position-relative">
          <div class="col-md-12 banner-img">
            <?php if ( $GLOBALS['SETTINGS']['CONTENT']['PROPERTY_BANNER_LINK_VALUE'] ) { ?>
            <a href="<?=$GLOBALS['SETTINGS']['CONTENT']['PROPERTY_BANNER_LINK_VALUE']?>">
            <?php } // if LINK ?>
            <img class="w-100 pc" alt="<? $APPLICATION->ShowTitle(); ?>" src="<?=CFile::GetPath($GLOBALS['SETTINGS']['CONTENT']['DETAIL_PICTURE'])?>" />
            <img class="w-100 mob" alt="<? $APPLICATION->ShowTitle(); ?>" src="<?=CFile::GetPath($GLOBALS['SETTINGS']['CONTENT']['PREVIEW_PICTURE'])?>" />
            <?php if ( $GLOBALS['SETTINGS']['CONTENT']['PROPERTY_BANNER_LINK_VALUE'] ) { ?>
            </a>
            <?php } // if LINK ?>
          </div>
          <div class="col-md-5 pt-4 px-5 pb-5 plate bg-darkblue">
            <div class="h3"><?=$GLOBALS['SETTINGS']['CONTENT']['PROPERTY_BANNER_H1_VALUE']?></div>
            <div class="h5 mb-3"><?=$GLOBALS['SETTINGS']['CONTENT']['PROPERTY_BANNER_H2_VALUE']?></div>
            <form data-event="callback">
            <input type="hidden" name="Form" value="Заказать обратный звонок" />
            <input type="hidden" name="Section" value="15" />
            <input type="hidden" name="Source" value="Баннер" />
            <div class="row text-left">
              <div class="col-md-12">
                <div class="form-group">
                  <input type="text" class="form-control" name="Name" placeholder="ФИО *" required />
                </div>
                <div class="form-group">
                  <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
                </div>
                <div class="form-group">
                  <select class="form-control" name="DC" required>
                    <option disabled selected>Дилерский центр *</option>
                    <? foreach ( $GLOBALS['SETTINGS']['DC'] as $i ) { ?>
                    <option value="<?=$i['NAME']?>"><?=$i['NAME']?></option>
                    <? } ?>
                  </select>
                </div>
                <div class="form-group my-3">
                  <a href="#" class="but-red btn-block p-2 text-center" role="Send">Оставить заявку</a>
                </div>
                <div class="form-group"><? include '_forms.personal.php'; ?></div>
              </div>
            </div>
          </form>
          <? include '_forms.result.php'; ?>
          </div>
        </div>
        
      </div>
    </div>